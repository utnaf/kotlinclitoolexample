import service.WordCountService
import java.io.File
import kotlin.system.exitProcess

fun main(args : Array<String>) {

    if(args.isEmpty()) {
        println("You must provide and argument with the filename")
        exitProcess(144);
    }

    val file = File(args[0])

    if(!file.exists()) {
        println("The file your provided doesn't exist")
        exitProcess(1)
    }

    val allTheWords = file.inputStream().bufferedReader().use { it.readText() }
    val wordsCounter = WordCountService()

    wordsCounter
        .count(allTheWords, 10)
        .forEach { println("${it.second} ${it.first}") }
}