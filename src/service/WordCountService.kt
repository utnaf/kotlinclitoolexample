package service

class WordCountService {
    fun count(allTheWords: String, top: Int = 5, maxWordLength: Int = 0): List<Pair<String, Int>> {

        val words = allTheWords
            .replace("[^A-z0-9 ']+".toRegex(), " ")
            .replace("  ", " ")
            .split(" ")

        val wordsMap = mutableMapOf<String, Int>();
        words.forEach {
            val normalizedWord = it.capitalize().trim('\'')
            if(wordsMap[normalizedWord] === null) {
                wordsMap[normalizedWord] = 0
            }

            wordsMap.computeIfPresent(normalizedWord) { _, v -> v + 1 }
        }

        // print a sorted list of the most 5 popular words in a text file
        return wordsMap
            .toList()
            .filter { it.first.length > maxWordLength }
            .sortedWith(compareBy { it.first })
            .sortedWith(compareByDescending { it.second })
            .subList(0, top);
    }
}